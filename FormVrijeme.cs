using Timer = System.Windows.Forms.Timer;

namespace VrijemeApp
{
    public partial class FormVrijeme : Form
    {
        private Timer timer;
        Vrijeme vrijeme;
        public FormVrijeme()
        {
            InitializeComponent();
            vrijeme = new Vrijeme();
            vrijeme.PodaciOsvjezeni += Vrijeme_PodaciOsvjezeni;
            //OsvjeziPodatke(DateTime.Now);

            //timer = new Timer();
            //timer.Interval = 300000; // 5 minuta u ms

            //// event handler
            //timer.Tick += Timer_Tick;

            //timer.Start();
            //OsvjeziPodatke();
        }
        private void Vrijeme_PodaciOsvjezeni(object sender, VrijemeArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    OsvjeziPodatke(e.OsvjezenoU);
                }));
                return;
            }
            else
            {
                OsvjeziPodatke(e.OsvjezenoU);
            }
        }
        //private void Timer_Tick(object? sender, EventArgs e)
        //{
        //    vrijeme = new Vrijeme();
        //    OsvjeziPodatke();
        //}

        private void OsvjeziPodatke(DateTime vrijemeOsvjezivanja)
        {
            OsvjeziPopisGradova();
            OsvjeziNajhladnijeINajtoplijeGradove();
            this.Text = $"VrijemeApp (last refreshed {vrijemeOsvjezivanja.ToString()})";
        }
        private void OsvjeziPopisGradova()
        {
            cbGrad.Items.Clear();
            cbGrad.Items.AddRange(vrijeme.DohvatiImenaGradova().ToArray());
        }

        private void PrikaziPodatkeZaGrad()
        {
            var podatak = vrijeme.DohvatiPodatkeZaGrad(cbGrad.Text);
            lblTemperatura.Text = $"{podatak.Temperatura} �C";
            lblVlaga.Text = $"{podatak.Vlaga} %";
            lblTlak.Text = $"{podatak.Tlak} hPa";
        }

        private void OsvjeziNajhladnijeINajtoplijeGradove()
        {
            lbNajtopliji.Items.Clear();
            lbNajhladniji.Items.Clear();
            lbNajtopliji.Items.AddRange(vrijeme.DohvatiNajtoplijeGradove().ToArray());
            lbNajhladniji.Items.AddRange(vrijeme.DohvatiNajhladnijeGradove().ToArray());
        }

        private void cbGrad_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblGrad.Text = cbGrad.Text;
            PrikaziPodatkeZaGrad();
        }
    }
}