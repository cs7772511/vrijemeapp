﻿namespace VrijemeApp
{
    internal class VrijemeArgs : EventArgs
    {
        public DateTime OsvjezenoU { get; set; }
        public string Poruka { get; set; }
        public VrijemeArgs(string poruka = "")
        {
            OsvjezenoU = DateTime.Now;
            Poruka = poruka;
        }
    }
}
