﻿using System.Data;
using System.Net;
using System.Xml;

namespace VrijemeApp
{
    internal class Vrijeme
    {
        public Vrijeme()
        {
            vrijeme = new XmlDocument();

            Task osvjezavanje = new Task(() =>
            {
                while (true)
                {
                    OsvjeziPodatke();
                    Thread.Sleep(10000);
                }
            });
            osvjezavanje.Start();

        }
        public void OsvjeziPodatke()
        {
            try
            {
                vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
                podaci = DohvatiPodatke();
                PodaciOsvjezeni?.Invoke(this, new VrijemeArgs());
            }
            catch (Exception ex)
            {
                GreskaPrilikomOsvjezavanja?.Invoke(this, new VrijemeArgs(ex.Message));
            }
        }

        public List<VrijemePodatak> DohvatiPodatke()
        {
            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            try
            {
                XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
                foreach (XmlNode grad in gradovi)
                {
                    string gradIme = grad["GradIme"]?.InnerText;
                    string temperatura = grad["Podatci"]?["Temp"]?.InnerText;
                    string vlaga = grad["Podatci"]?["Vlaga"]?.InnerText;
                    string tlak = grad["Podatci"]?["Tlak"]?.InnerText;

                    if (gradIme == null || temperatura == null || vlaga == null || tlak == null)
                    {
                        throw new FormatException("Podaci za grad " + grad["GradIme"].InnerText + " su neispravni.");
                    }

                    podaci.Add(new VrijemePodatak(
                        gradIme,
                        temperatura,
                        vlaga,
                        tlak
                    ));
                }
            }
            catch (XmlException ex)
            {
                throw new DataException("Greška prilikom parsiranja XML dokumenta.", ex);
            }
            catch (WebException ex) // vjerojatno nepotrebno
            {
                throw new Exception("Greška prilikom preuzimanja podataka sa web stranice.", ex);
            }

            if (podaci.Count == 0)
            {
                throw new DataException("Nema dostupnih podataka.");
            }

            return podaci;
        }
        public IEnumerable<string> DohvatiImenaGradova()
        {
            return
                from p in podaci
                orderby p.Grad
                select p.Grad;
        }

        public VrijemePodatak DohvatiPodatkeZaGrad(string grad)
        {
            return
                (from p in podaci
                 where p.Grad == grad
                 select p).First();
        }
        public IEnumerable<string> DohvatiNajtoplijeGradove()
        {
            return
               (from p in podaci
                orderby double.Parse(p.Temperatura) descending
                select p.ToString()).Take(5);
        }
        public IEnumerable<string> DohvatiNajhladnijeGradove()
        {
            return
               (from p in podaci
                orderby double.Parse(p.Temperatura) ascending
                select p.ToString()).Take(5);
        }

        private XmlDocument vrijeme;
        private List<VrijemePodatak>? podaci;

        public delegate void VrijemeDelegate(object sender, VrijemeArgs e);
        public event VrijemeDelegate PodaciOsvjezeni;
        public event VrijemeDelegate GreskaPrilikomOsvjezavanja;


    }
}
